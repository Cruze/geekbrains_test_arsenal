package com.itlavka.geekbrains.data.repo;

import com.itlavka.geekbrains.data.entity.CourseEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CourseRepository extends CrudRepository<CourseEntity, Integer> {

    List<CourseEntity> getByPriceGreaterThan(Double price);

}
