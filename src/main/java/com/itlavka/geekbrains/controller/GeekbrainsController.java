package com.itlavka.geekbrains.controller;

import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/geekbrains/students")
@CrossOrigin(origins = "*")
public class GeekbrainsController {

    private final Map<String, String> students = Map.of("1", "Oleg", "2", "Ivan", "3", "Olga");

    @SneakyThrows
    @GetMapping
    public ResponseEntity<List<String>> getAll() {

        return ResponseEntity.ok(new ArrayList<>(students.values()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> getById(@PathVariable String id) {
        String student = students.get(id);
        return ResponseEntity.ok(student);
    }

}
