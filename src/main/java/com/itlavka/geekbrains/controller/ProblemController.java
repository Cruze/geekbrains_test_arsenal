package com.itlavka.geekbrains.controller;

import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/problems")
@CrossOrigin(origins = "*")
public class ProblemController {

    @GetMapping("/outofmemory")
    public ResponseEntity<Void> outofmemory() {
        Map<String, CourseDto> map = new HashMap<>();
        for (int i = 0; i < 100_000_000; i++) {
            map.put(Integer.toString(i), new CourseDto("student"));
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping("/stackoverflow")
    public ResponseEntity<Void> stackOverflow() {
        hello();
        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    private void hello() {
        Thread.sleep(1000);
        hello();
    }
}
