package com.itlavka.geekbrains.soap;

import io.spring.guides.gs_producing_web_service.Course;
import io.spring.guides.gs_producing_web_service.GetCourseRequest;
import io.spring.guides.gs_producing_web_service.GetCourseResponse;
import io.spring.guides.gs_producing_web_service.Level;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class CoursesEndpoint {
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCourseRequest")
    @ResponsePayload
    public GetCourseResponse getCourse(@RequestPayload GetCourseRequest request) {
        GetCourseResponse response = new GetCourseResponse();
        Course course = new Course();
        course.setName(request.getName());
        course.setLevel(Level.SENIOR);
        course.setPrice(100_000);
        response.setCourse(course);
        return response;
    }

}
