package com.itlavka.geekbrains.websocket;

import lombok.SneakyThrows;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

@Controller
public class GeekbrainsQuestionWebSocket {

    @SneakyThrows
    @MessageMapping("/questions")
    @SendTo("/topic/answers")
    public String askQuestion(String question) {
        Thread.sleep(500);
        return HtmlUtils.htmlEscape(question) + " Будущее в тумане...";
    }

}
