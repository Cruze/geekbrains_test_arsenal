package com.itlavka.geekbrains.businesslogic;


import java.math.BigDecimal;
import java.util.List;

public interface DiscountService {
    BigDecimal getDiscountAmount(List<Course> courses, Integer discountPercent);
}
