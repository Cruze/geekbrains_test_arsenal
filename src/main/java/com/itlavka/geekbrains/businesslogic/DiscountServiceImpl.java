package com.itlavka.geekbrains.businesslogic;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService {

    public BigDecimal getDiscountAmount(List<Course> courses, Integer discountPercent) {
        BigDecimal price = courses.stream()
                .map(Course::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return price.divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP)
                .multiply(BigDecimal.valueOf(discountPercent));
    }

}
