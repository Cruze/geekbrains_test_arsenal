package com.itlavka.geekbrains.businesslogic;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;


@Data
@Builder
public class Course {
    private String name;
    private BigDecimal price;
    
}
