package com.itlavka.geekbrains.soap;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.xml.transform.StringSource;
import reactor.core.publisher.Mono;

import javax.xml.transform.Source;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class CoursesEndpointTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void test() {

        Source requestPayload = new StringSource("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
                "                  xmlns:gs=\"http://spring.io/guides/gs-producing-web-service\">\n" +
                "    <soapenv:Header/>\n" +
                "    <soapenv:Body>\n" +
                "        <gs:getCourseRequest>\n" +
                "            <gs:name>Spring</gs:name>\n" +
                "        </gs:getCourseRequest>\n" +
                "    </soapenv:Body>\n" +
                "</soapenv:Envelope>");

        Source responsePayload = new StringSource("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"><SOAP-ENV:Header/><SOAP-ENV:Body><ns2:getCourseResponse xmlns:ns2=\"http://spring.io/guides/gs-producing-web-service\"><ns2:course><ns2:name>Spring</ns2:name><ns2:price>100000" +
                "</ns2:price><ns2:level>SENIOR</ns2:level></ns2:course>" +
                "</ns2:getCourseResponse></SOAP-ENV:Body></SOAP-ENV:Envelope>");

        webTestClient.get().uri("/ws/courses.wsdl")
                .exchange().expectStatus().isOk();

        webTestClient.post().uri("/ws")
                .contentType(MediaType.TEXT_XML)
                .accept(MediaType.TEXT_XML)
                .body(Mono.just(requestPayload.toString()), String.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody().xml(responsePayload.toString());

    }

}
