package com.itlavka.geekbrains.businesslogic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = DiscountServiceImpl.class)
class DiscountServiceTest {

    @Autowired
    private DiscountService discountService;

    @Test
    void whenNoCourses() {
        //given
        List<Course> courses = new ArrayList<>();
        Integer discount = 0;

        //when
        BigDecimal actual = discountService.getDiscountAmount(courses, discount);

        //then
        Assertions.assertEquals(BigDecimal.ZERO, actual);
    }

    @Test
    void whenNoDiscount() {
        //given
        Course springCourse = Course.builder()
                .name("springCourse")
                .price(BigDecimal.valueOf(100_000))
                .build();
        List<Course> courses = List.of(springCourse);
        Integer discount = 0;

        //when
        BigDecimal actual = discountService.getDiscountAmount(courses, discount);

        //then
        Assertions.assertEquals(BigDecimal.ZERO, actual);
    }

    @Test
    void whenDiscountForOneCourse() {
        //given
        Course springCourse = Course.builder()
                .name("springCourse")
                .price(BigDecimal.valueOf(100_000))
                .build();
        List<Course> courses = List.of(springCourse);
        Integer discount = 10;

        //when
        BigDecimal actual = discountService.getDiscountAmount(courses, discount);

        //then
        Assertions.assertEquals(BigDecimal.valueOf(10_000), actual);
    }

    @Test
    void whenDiscountForCourses() {
        //given
        Course springCourse = Course.builder()
                .name("springCourse")
                .price(BigDecimal.valueOf(100_000))
                .build();

        Course angularCourse = Course.builder()
                .name("springCourse")
                .price(BigDecimal.valueOf(50_000))
                .build();
        List<Course> courses = List.of(springCourse, angularCourse);
        Integer discount = 10;

        //when
        BigDecimal actual = discountService.getDiscountAmount(courses, discount);

        //then
        Assertions.assertEquals(BigDecimal.valueOf(15_000), actual);
    }

}