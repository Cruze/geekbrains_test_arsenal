package com.itlavka.geekbrains.selenium;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SeleniumTest {

    @Test
    void test() {
        //given
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        //when
        driver.get("https://geekbrains.ru/");

        driver.findElement(By.className("gb-header__nav_item")).click();

        List<WebElement> elements = driver.findElements(By.cssSelector("div[data-testid=promocode-banner-container]"));

        //then
//        Assertions.assertEquals("Курсы | GeekBrains - образовательный портал", driver.getTitle());
        Assertions.assertFalse(elements.isEmpty());
        driver.close();
    }
}
