package com.itlavka.geekbrains.websocket;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static java.util.concurrent.TimeUnit.SECONDS;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GeekbrainsQuestionWebSocketTest {

    @LocalServerPort
    private int localServerPort;

    private WebSocketStompClient webSocketStompClient;

    @BeforeEach
    public void setup() {
        this.webSocketStompClient = new WebSocketStompClient(new SockJsClient(
                List.of(new WebSocketTransport(new StandardWebSocketClient()))));
    }

    @SneakyThrows
    @Test
    void test() {
        //given
        String url = "ws://localhost:" + localServerPort + "/gs-guide-websocket";

        webSocketStompClient.setMessageConverter(new StringMessageConverter());

        ListenableFuture<StompSession> connect = webSocketStompClient.connect(url, new StompSessionHandlerAdapter() {});

        StompSession session = connect.get(1, SECONDS);

        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(1);


        session.subscribe("/topic/answers", new StompFrameHandler() {
            @Override
            public Type getPayloadType(StompHeaders headers) {
                return String.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                //then
                blockingQueue.add((String) payload);
            }
        });

        //when
        session.send("/app/questions", "Сколько будет стоить биткоин?");

        //then
        String actual = blockingQueue.poll(1, SECONDS);
        Assertions.assertEquals("Сколько будет стоить биткоин? Будущее в тумане...", actual);

    }

}