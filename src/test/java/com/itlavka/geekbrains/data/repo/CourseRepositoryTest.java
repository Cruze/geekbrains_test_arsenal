package com.itlavka.geekbrains.data.repo;

import com.itlavka.geekbrains.data.entity.CourseEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

@DataJpaTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepository;

    @Test
    void test() {
        //given
        CourseEntity springCourse = CourseEntity.builder()
                .name("spring")
                .price(110d)
                .build();

        CourseEntity angularCourse = CourseEntity.builder()
                .name("angularCourse")
                .price(90d)
                .build();

        courseRepository.save(springCourse);
        courseRepository.save(angularCourse);

        //when
        List<CourseEntity> actual = courseRepository.getByPriceGreaterThan(100d);

        //then
        Assertions.assertEquals(1, actual.size());
        Assertions.assertEquals("spring", actual.get(0).getName());
    }

}